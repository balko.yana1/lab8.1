//2. Об’єкт “ДАІ” (Код, ПІБ власника машини; марка, номер машини; колір.

function Trafficpolice(code, fullname, carbrand,carnumber, color) {
    this.code = code;
    this.fullname = fullname;
    this.carbrand = carbrand;
    this.carnumber = carnumber;
    this.color = color;
    this.printInf = function() {
        console.log(`Код: ${this.code}`);
        console.log(`ПІБ власника машини: ${this.fullname}`);
        console.log(` марка: ${this.carbrand}`);
        console.log(`номер машини: ${this.carnumber}`);
        console.log(`колір: ${this.color}`);
    }
}
const car1 = new Trafficpolice('Код','ПІБ власника машини',' марка','номер машини','колір');
const car2 = new Trafficpolice('Код','ПІБ власника машини',' марка','номер машини','колір');
const car3 = new Trafficpolice('Код','ПІБ власника машини',' марка','номер машини','колір');
car1.printInf();
car2.printInf();
car3.printInf();

//9.  Об’єкт “Аукціон” (Код, назва лота, дата початку торгів, дата
//завершення торгів, стартова ціна, кінцева ціна).
function Auction(code, name, dateofstart,dateofend, startingprice,finalprice) {
    this.code = code;
    this.name = name;
    this.dateofstart = dateofstart;
    this.dateofend = dateofend;
    this.startingprice = startingprice;
    this.finalprice = finalprice;
    this.printInf = function() {
        console.log(`Код: ${this.code}`);
        console.log(`назва лота: ${this.name}`);
        console.log(` дата початку торгів: ${this.dateofstart}`);
        console.log(`дата завершення торгів: ${this.dateofend}`);
        console.log(`стартова ціна: ${this.startingprice}`);
        console.log(`кінцева ціна: ${this.finalprice}`);
    }
}
const lot1 = new Trafficpolice('Код','назва лота',' дата початку торгів','дата завершення торгів','стартова ціна','кінцева ціна');
const lot2 = new Trafficpolice('Код','назва лота',' дата початку торгів','дата завершення торгів','стартова ціна','кінцева ціна');
const lot3 = new Trafficpolice('Код','назва лота',' дата початку торгів','дата завершення торгів','стартова ціна','кінцева ціна');
lot1.printInf();
lot2.printInf();
lot3.printInf();
//1. Об’єкт “Бухгалтерія” (Код, ПІБ; посада; заробітна плата; кількість дітей;
//стаж).
function Accounting(code, fullname, position,salary, numberofchildren,experience) {
    this.code = code;
    this.fullname = fullname;
    this.position = position;
    this.salary = salary;
    this.numberofchildren = numberofchildren;
    this.experience = experience;
    this.printInf = function() {
        console.log(`Код: ${this.code}`);
        console.log(`ПІБ: ${this.fullname}`);
        console.log(` посада: ${this.position}`);
        console.log(`заробітна плата: ${this.salary}`);
        console.log(`кількість дітей: ${this.numberofchildren}`);
        console.log(`стаж: ${this.experience}`);
    }
}
const person1 = new Accounting('Код','ПІБ','посада','заробітна плата','кількість дітей','стаж');
const person2 = new Accounting('Код','ПІБ','посада','заробітна плата','кількість дітей','стаж');
const person3 = new Accounting('Код','ПІБ','посада','заробітна плата','кількість дітей','стаж');
person1.printInf();
person2.printInf();
person3.printInf();

